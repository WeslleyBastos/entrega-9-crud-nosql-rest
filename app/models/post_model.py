import pymongo
from datetime import datetime
from flask import jsonify
from app.exceptions.post_exceptions import InvalidPost, PostNotFound

client = pymongo.MongoClient("mongodb://localhost:27017/")

db = client["kenzie"]

class Post:
    def __init__(self, title: str, author: str, tags: list, content:str):
        self.title = title
        self.author = author
        self.tags = tags
        self.content = content
        self.id = Post.get_id() + 1
        self.created_at = datetime.utcnow()
        self.update_at = None

    
    def create_post(self):
        new_post = db.posts.insert_one(self.__dict__)

        if not new_post:
            raise InvalidPost
        
        post = db.posts.find_one({'id': self.id})
        del post['_id']
        del post['update_at']

        return post

    @staticmethod
    def update(old_post, new_post):
        new_post['update_at'] = datetime.utcnow()
        post_updated = db.post.update_one({'id': old_post['id']}, {'$set': new_post})

        if not post_updated:
            raise PostNotFound
        
        post = Post.get_id(old_post['id'])
        return post

    @staticmethod
    def get_all_posts():
        all_posts = list(db.posts.find())
        
        for post in all_posts:
            del post['_id']
            if not post['update_at']:
                del post['update_at']
        return all_posts

    @staticmethod
    def get_one_post(id: int):
        post = db.posts.find_one({'id': id})

        if not post:
            raise PostNotFound

        del post['_id']
        
        if not post['update_at']:
            del post['update_at']
        
        return post
    
    @staticmethod
    def get_id():
        list_data = Post.get_all_posts()

        if len(list_data) > 0:
            next_id = list_data[-1]['id']
            return next_id
        
        return 0
        
    @staticmethod
    def delete_post(id: int):
        post = db.posts.find_one_and_delete({'id': id})

        if not post:
            raise PostNotFound

        del post['_id']
        return post
    
    def return_info(self):
        return {
            "id": self.id,
            "created_at": self.created_at,
            "update_at": self.update_at,
            "title": self.title,
            "author": self.author,
            "tags": self.tags,
            "content": self.content
        }
