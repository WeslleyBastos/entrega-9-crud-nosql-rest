from flask import Flask, request, jsonify
from app.exceptions.post_exceptions import InvalidPost, PostNotFound
from app.models.post_model import Post

def home_view(app: Flask):

    @app.post('/posts')
    def create_post():
        data = request.get_json()

        try:
            posts = Post(**data)
            new_post = posts.create_post()

            return new_post, 201
        except InvalidPost:
            return {'message': 'Os dados são inválidos para criar a publicação. Favor revisar os dados.'}, 400

    @app.get('/posts')
    def get_post():
        list_posts = Post.get_all_posts()
        return jsonify(list_posts), 200
    
    @app.get('/posts/<int:id>')
    def get_post_by_id(id:int):
        try:
            post = Post.get_one_post(id)
            return post, 200
        except PostNotFound:
            return {'message': 'A publicação não foi encontrada! Favor verifique os dados.'}, 404


    @app.delete('/posts/<int:id>')
    def delete_post(id: int):
        try:
            post = Post.delete_post(id)
            return post, 200
        except PostNotFound:
            return {'message': "O post não foi encontado!"}, 404

    @app.patch('/posts/<int:id>')
    def update_post(id:int):
        data = request.json

        try:
            post = Post.get_id(id)
            post_update = Post.update(post, data)
            return post_update, 200
        except PostNotFound:
            return {'message': 'A publicação não foi encontrada! Por favor, verifique os dados.'}, 404
        except TypeError:
            return {'message': 'Os dados são inválidos. Por favor, verifique seus dados.'}, 400
    
    return app